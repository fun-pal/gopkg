package logs

type ConfigOption interface {
	apply(*config)
}

type coverOption struct {
	f func(*config)
}

func (c coverOption) apply(cfg *config) {
	c.f(cfg)
}

func newFuncOption(f func(*config)) coverOption {
	return coverOption{
		f: f,
	}
}
