package logs

func Debug(args ...interface{}) {
	debugLogger.Sugar().Debug(args...)
}

func Debugf(template string, args ...interface{}) {
	debugLogger.Sugar().Debugf(template, args...)
}

func Info(args ...interface{}) {
	infoLogger.Sugar().Info(args...)
}

func Infof(template string, args ...interface{}) {
	infoLogger.Sugar().Infof(template, args...)
}

func Warn(args ...interface{}) {
	warnLogger.Sugar().Warn(args...)
}

func Warnf(template string, args ...interface{}) {
	warnLogger.Sugar().Warnf(template, args...)
}

func Error(args ...interface{}) {
	errorLogger.Sugar().Error(args...)
}

func Errorf(template string, args ...interface{}) {
	errorLogger.Sugar().Errorf(template, args...)
}

func Panic(args ...interface{}) {
	panicLogger.Sugar().Panic(args...)
}

func Panicf(template string, args ...interface{}) {
	panicLogger.Sugar().Panicf(template, args...)
}
