# 基于 zap 日志库封装自己的日志组件

## （1）直接使用

```go
package main

import "gitee.com/fun-pal/gopkg/logs"

func main() {
	logs.Info("test...")
}
```

## （2）自定义 logs 配置

```go
package main

import "gitee.com/fun-pal/gopkg/logs"

func main() {
	// 自定义 config信息
	logs.InitWithConfig(logs.NewConfig(
		logs.WithServiceName("my_service"),
		logs.WithPath("my_logs/"),
	))
	logs.Info("test...")
}
```
