package logs

import "strings"

func WithServiceName(serviceName string) ConfigOption {
	return newFuncOption(func(c *config) {
		c.serviceName = serviceName
	})
}

func WithPath(path string) ConfigOption {
	// 如果没有传入 / 作为路径分割，则使用默认
	if !strings.Contains(path, "/") {
		return newFuncOption(func(c *config) {
			c.path = defaultConfigOption().path
		})
	}
	return newFuncOption(func(c *config) {
		c.path = path
	})
}

func WithMaxsize(max int) ConfigOption {
	return newFuncOption(func(c *config) {
		c.maxSize = max
	})
}

func WithMaxAge(max int) ConfigOption {
	return newFuncOption(func(c *config) {
		c.maxAge = max
	})
}

func WithLevel(level string) ConfigOption {
	return newFuncOption(func(c *config) {
		c.level = level
	})
}

// Deprecated
func withMode(mode string) ConfigOption {
	return newFuncOption(func(c *config) {
		//c.mode = mode
	})
}

// Deprecated
func withCompress(compress bool) ConfigOption {
	return newFuncOption(func(c *config) {
		//c.compress = compress
	})
}

// Deprecated
func withMaxBackups(max int) ConfigOption {
	return newFuncOption(func(c *config) {
		//c.maxBackups = max
	})
}
