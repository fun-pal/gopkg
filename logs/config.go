package logs

type config struct {
	serviceName string `json:"service_name"` // 服务名称
	path        string `json:"path"`         // 日志路径
	maxSize     int    `json:"max_size"`     // 最大容量
	maxAge      int    `json:"max_age"`      // 最长保存时间
	level       string `json:"level"`        // 日志等级 [debug, info, warn, error, panic]
	//mode        string `json:"mode"`         // 模式
	//compress    bool   `json:"compress"`     // 是否压缩，默认为 false
	//maxBackups  int    `json:"max_backups"`  // 最大备份数量
}

func NewConfig(opts ...ConfigOption) *config {
	var cfg config
	for _, opt := range opts {
		opt.apply(&cfg)
	}
	return &cfg
}

// 默认的日志配置选项
func defaultConfigOption() *config {
	return &config{
		serviceName: "",
		path:        "log/",
		maxSize:     1024 * 1024 * 100,
		maxAge:      14,
		level:       "debug",
		//maxBackups:  7,
		//compress:    true,
	}
}
