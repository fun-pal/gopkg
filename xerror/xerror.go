package xerror

const (
	paramsFailedCode = 5000
	paramsFailedMsg  = "参数有误，请检查后重试"

	failedCode = 1000
	failedMsg  = "服务开小差啦，请稍后再试"
)

// CustomError 实现error接口
type CustomError struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
}

func (e *CustomError) Error() string {
	return e.Msg
}

// NewErrorWithCode 返回自定义的 错误码 + 错误信息
func NewErrorWithCode(code int, msg string) error {
	return &CustomError{Code: code, Msg: msg}
}

// NewErrorWithMsg 返回自定义的 错误信息
func NewErrorWithMsg(msg string) error {
	if msg == "" {
		return &CustomError{Code: failedCode, Msg: failedMsg}
	}
	return &CustomError{Code: failedCode, Msg: msg}
}

// NewParamsError 接口参数校验错误返回
func NewParamsError(msg string) error {
	if msg == "" {
		return NewErrorWithCode(paramsFailedCode, paramsFailedMsg)
	}
	return NewErrorWithCode(paramsFailedCode, msg)
}
